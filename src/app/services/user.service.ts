import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { IndexeddbService } from './idb.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  public userEmitter$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  data = [];
  constructor(private http: HttpClient, private idbService: IndexeddbService) {}

  public getAllUser(limit: any, page: any): Promise<any[]> {
    return new Promise(async (resolve) => {
      //get token to local storage
      let token: any;
      await this.idbService.get('token').then((tok) => {
        token = tok;
      });
      // console.log(token);
      //use token to API
      this.http
        .get(`${environment.urlAPI}/user/list?limit=${limit}&page=${page}`, {
          headers: {
            authorization: token,
          },
        })
        .subscribe((response) => {
          resolve(response as any[]);
        });
    });
  }

  public addUser(data: []) {
    return this.http.post(`${environment.urlAPI}/register`, data).subscribe(
      (res: any) => {
        console.log(res);
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    );
  }

  public getToken() {
    let token: any;
    this.idbService.get('token').then((tok) => {
      return (token = tok);
    });
  }

  public getUserById(id: any): Promise<any[]> {
    return new Promise(async (resolve) => {
      //get token to local storage
      let token: any;
      await this.idbService.get('token').then((tok) => {
        token = tok;
      });
      // console.log(token);
      //use token to API
      this.http
        .get(`${environment.urlAPI}/user/${id}`, {
          headers: {
            authorization: token,
          },
        })
        .subscribe((response) => {
          resolve(response as any[]);
          // console.log(response);
        });
    });
  }
  public updateUser(id: any, data: any): Promise<any[]> {
    return new Promise(async (resolve) => {
      //get token to local storage
      let token: any;
      await this.idbService.get('token').then((tok) => {
        token = tok;
      });
      // console.log(token);
      //use token to API
      this.http
        .put(`${environment.urlAPI}/user/${id}/edit`, data, {
          headers: {
            authorization: token,
          },
        })
        .subscribe((response: any) => {
          resolve(response);
        });
    });
  }
  public deleteUser(id: any): Promise<any[]> {
    return new Promise(async (resolve) => {
      //get token to local storage
      let token: any;
      await this.idbService.get('token').then((tok) => {
        token = tok;
      });
      // console.log(token);
      //use token to API
      this.http
        .delete(`${environment.urlAPI}/user/${id}`, {
          headers: {
            authorization: token,
          },
        })
        .subscribe((response) => {
          resolve(response as any[]);
          // console.log(response);
        });
    });
  }
}
