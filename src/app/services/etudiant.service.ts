import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IndexeddbService } from './idb.service';

@Injectable({
  providedIn: 'root',
})
export class EtudiantService {
  constructor(private http: HttpClient, private idbService: IndexeddbService) {}

  public getAllStudent(): Promise<any[]> {
    return new Promise(async (resolve) => {
      //get token to local storage
      let token: any;
      await this.idbService.get('token').then((tok) => {
        token = tok;
      });
      // console.log(token);
      //use token to API
      this.http
        .get(`${environment.urlAPI}/student/list`, {
          headers: {
            authorization: token,
          },
        })
        .subscribe((response) => {
          resolve(response as any[]);
        });
    });
  }
}
