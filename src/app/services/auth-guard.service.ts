import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { IndexeddbService } from './idb.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService implements CanActivate {
  public loginEmitter$: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(
    false
  );

  public isvalide: any;
  constructor(public router: Router, private idbService: IndexeddbService) {}
  async canActivate(): Promise<boolean> {
    await this.idbService.get('userDetail').then((res: any) => {
      if (res !== undefined) {
        // console.log('miova: ', res.isValide);
        this.isvalide = res.isValide;
        this.loginEmitter$.next(this.isvalide);
      }
    });
    if (this.isvalide !== true || this.isvalide === undefined) {
      // console.log('not authorized');
      this.router.navigate(['login']);
      return false;
    }
    // console.log('authorized');
    return true;
  }

  async getIsActivated() {}
}
