import { Injectable } from '@angular/core';
import { openDB } from 'idb';

@Injectable({
  providedIn: 'root',
})
export class IndexeddbService {
  constructor() {}
  public openDB(dbname: any) {
    return openDB('eniStorage', 1, {
      upgrade(db) {
        db.createObjectStore(dbname);
      },
    });
  }
  //image
  async get(key: string) {
    return (await this.openDB('tmp')).get('tmp', key);
  }
  //image
  async set(key: string, val: any) {
    return (await this.openDB('tmp')).put('tmp', val, key);
  }
  //set JSON in idb
  async setJson(key: string, val: JSON) {
    return (await this.openDB('tmp')).put('tmp', val, key);
  }
  //remove object in idb
  public async remove(key: string) {
    return (await this.openDB('tmp')).delete('tmp', key);
  }
  //image
  async delete(key: string) {
    return (await this.openDB('tmp')).delete('tmp', key);
  }
  //image
  async clear() {
    return (await this.openDB('tmp')).clear('tmp');
  }
  //image
  async keys() {
    return (await this.openDB('tmp')).getAllKeys('tmp');
  }
}
