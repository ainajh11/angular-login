import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { IndexeddbService } from '../../services/idb.service';
import { UserService } from '../../services/user.service';
import { AuthGuardService } from '../../services/auth-guard.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  public formGroup: any = FormGroup;
  public data: any;
  constructor(
    private authService: AuthService,
    private idbService: IndexeddbService,
    private userService: UserService,
    private router: Router,
    private authGuard: AuthGuardService
  ) {}

  ngOnInit(): void {
    this.initForm();
  }
  initForm() {
    this.formGroup = new FormGroup({
      login: new FormControl('2800', [Validators.required]),
      password: new FormControl('password', [Validators.required]),
    });
  }
  onSubmit() {
    if (this.formGroup.valid) {
      this.authService.login(this.formGroup.value).subscribe((res) => {
        //add to idb storage when key is "token"
        const token = 'bear ' + res.token;
        this.addToken(token);

        // add to key "userDetail"
        const userId = res.userId;
        this.userService.getUserById(userId).then(async (respo: any) => {
          this.data = respo.data;
          // console.log(this.data);
          if (this.data['accountType'] === 'ADMIN') {
            this.addUserDetail({ data: this.data, isValide: true });
            await this.authGuard.getIsActivated();
            this.router.navigate(['/']);
            this.authGuard.loginEmitter$.next(true);
          }
        });
      });
    }
  }

  addToken(token: any) {
    this.idbService.set('token', token);
  }
  addUserDetail(data: any) {
    this.idbService.set('userDetail', data);
  }
}
