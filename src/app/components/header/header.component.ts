import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IndexeddbService } from 'src/app/services/idb.service';
import { AuthGuardService } from '../../services/auth-guard.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public isAuth = false;
  constructor(
    private idbService: IndexeddbService,
    private router: Router,
    private authG: AuthGuardService
  ) {}

  ngOnInit(): void {
    this.authG.loginEmitter$.subscribe((res) => {
      console.log('sssss', res);
      if (res) {
        this.isAuth = true;
      } else {
        this.isAuth = false;
      }
    });
  }
  logOut() {
    this.router.navigate(['login']);
    // this.authG.loginEmitter$.next(false);
    this.addUserDetail({ isValide: false });
  }

  addUserDetail(data: any) {
    this.idbService.set('userDetail', data);
  }
}
