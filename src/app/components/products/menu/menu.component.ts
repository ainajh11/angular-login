import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  isExpanded: Boolean = true;

  constructor() {}

  ngOnInit(): void {}

  showHide() {
    this.isExpanded = !this.isExpanded;
  }
}
